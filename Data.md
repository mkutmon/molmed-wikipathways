# Preparation

Download the [data.zip](https://gitlab.com/mkutmon/molmed-wikipathways/-/raw/master/data/data.zip?inline=false) file (right-click > save link as) containing all the additional files required for the hands-on sessions about pathway and network analysis. Make sure you unzip the file.

Ideally you copy the pathvisio folder, the .bridge file and the human pathway collection that you downloaded during the installation also in the PathwayAnalysis folder within the data folder so you can easily find all the required data.