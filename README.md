## WikiPathways practical
MolMed Course: Gene expression data analysis using R: How to make sense out of your RNA-Seq/microarray data

<img src="images/logos.png"  width="350">

---


*  **Contact information:** Martina Summer-Kutmon (martina.kutmon (a) maastrichtuniversity.nl)
*  **Installation instructions:** [Installation-instructions.pdf](https://gitlab.com/mkutmon/molmed-wikipathways/raw/master/data/Installation-instructions.pdf)

Follow the installation instructions and make sure, you have downloaded **PathVisio**, the latest **WikiPathways human pathway collection** and the latest **BridgeDb human mapping** file.
Cytoscape is needed for the second part of the practical.

---

In this practical, you will learn the basic applications of pathway analysis by investigating a gene expression lung cancer dataset from TCGA (cancer-vs-healthy-tissue). 

*  Part 1: You will learn how to perform pathway analysis using transcriptomics data in PathVisio. We will focus on data visualization, pathway statistics and biological interpretation.
*  Part 2: Using Cytoscape and the automation feature, you will learn how to open pathways and visualize data using Cytoscape.

Feel free to work alone or in pairs. Step-by-step instructions are provided. In between there are questions related to the methodology or biological interpretation. The aim of these questions is to make you really think about the analysis you are performing and the produced results. 

You can downloaded the example lung cancer dataset here: [lung-cancer-data.txt](https://gitlab.com/mkutmon/molmed-wikipathways/raw/master/data/lung-cancer-data.txt).

---

### Part 1: Investigating the molecular processes altered in lung cancer

You will get to know WikiPathways and PathVisio and find out how to perform pathway analysis and visualization.<br/>
Find the step-by-step instructions [here](pathvisio.md).

---

### Part 2: Automated pathway analysis in R

You can also automatize a lot of the pathway analysis steps in R.<br/>
Find more information [here](automation.md).

---

### Afternoon session on Network Analysis

After a short introduction lecture on network biology in the afternoon, you will work on two examples on how you can use network biology to further analyse your gene expression data.
Find more information [here](networks.md).
