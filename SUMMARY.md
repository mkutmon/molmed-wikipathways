# Summary

* [Home](README.md)
* [Installation](installation.md)
* [Data download](Data.md)
* Morning: Pathway analysis
  * [Part 1: PathVisio](pathvisio.md)
  * [Part 2: Automation in R](automation.md)
* Afternoon: Network analysis
  * [Networks Part 1](network1.md)
  * [Networks Part 2](network2.md)