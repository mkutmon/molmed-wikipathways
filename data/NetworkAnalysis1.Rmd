---
  title: "Network Analysis Example 1"
author: "by Martina Kutmon"
package: rWikiPathways
date: "`r Sys.Date()`"
output: 
  BiocStyle::html_document:
  toc_float: true
---
  ```{r, echo = FALSE}
knitr::opts_chunk$set(
  eval=FALSE
)
```

This vignette will cover a wide range of analytical and visualization techniques involved in a typical pathway analysis. The **Overview** section will go into more detail on the particulars, but note that this vignette is designed to be modular and carefully considered. Please do not simply run the entire script and expect to get anything meaningful from the final output. This is an instructional device, ideal for guided workshops.

# Installation
First, make sure you have *rWikiPathways* installed...
```{r}
if (!requireNamespace("BiocManager", quietly = TRUE)) 
  install.packages("BiocManager")
if(!"rWikiPathways" %in% installed.packages())
  BiocManager::install("rWikiPathways", update = FALSE)
if(!"RCy3" %in% installed.packages())
  BiocManager::install("RCy3", update = FALSE)
if(!"org.Hs.eg.db" %in% installed.packages())
  BiocManager::install("org.Hs.eg.db", update = FALSE)
if(!"clusterProfiler" %in% installed.packages())
  BiocManager::install("clusterProfiler", update = FALSE)
if(!"DOSE" %in% installed.packages())
  BiocManager::install("DOSE", update = FALSE)
if(!"RColorBrewer" %in% installed.packages())
  BiocManager::install("RColorBrewer", update = FALSE)

library(rWikiPathways)
library(RCy3)
library(org.Hs.eg.db)
library(clusterProfiler)
library(DOSE)
library(RColorBrewer)
```

The **RCy3** package is used to connect with **Cytoscape**. So you will also need to install and launch Cytoscape:
  
  * Download the latest Cytoscape from https://cytoscape.org/download.html 
* Complete installation wizard
* Launch Cytoscape

```{r}
cytoscapePing()  #this will tell you if you're able to successfully connect to Cytoscape or not
```

For this vignette, you'll also need a couple *apps* for Cytoscape. With Cytoscape running, you can install each of these from the Cytoscape App Store with a single click:

* http://apps.cytoscape.org/apps/wikipathways
* http://apps.cytoscape.org/apps/cytargetlinker 

If you are running Cytoscape 3.7.0 or above, you can simply run these commands:
```{r}
installApp('WikiPathways') 
installApp('CyTargetLinker') 
installApp('stringApp') 
```

# Open pathway in Cytoscape
If you've got everything loaded and running, then all you need to do is run this command to import a pathway as a network into Cytoscape from WikiPathways and visualize the gene expression on the nodes.

```{r}
RCy3::commandsRun('wikipathways import-as-network id=WP179') 
lung.expr <- read.csv(system.file("extdata","data-lung-cancer.csv", package="rWikiPathways"),stringsAsFactors = FALSE)
colnames(lung.expr)[1] <- "GeneID"
loadTableData(lung.expr, data.key.column = "GeneID", table.key.column = "Ensembl") 

data.values = c(-3,0,3)
node.colors <- c(rev(brewer.pal(length(data.values), "RdBu")))

setNodeColorMapping("log2FC", data.values, node.colors, default.color = "#FFFFFF", style.name = "WikiPathways-As-Network") 
```

There it is! The latest approved version of the pathway, now in Cytoscape as a network model with annotated genes, proteins and metabolites. For performance reasons, Cytoscape sets a view threshold to hide details (like node labels) when zoomed out. If you want to override this, use...

```{r}
toggleGraphicsDetails()
```

Now that our pathways are loaded into Cytoscape, this opens up a ton of potential analysis and visualizations options! Check out the [Cytoscape manual](http://manual.cytoscape.org) and [App Store](http://apps.cytoscape.org/), for starters. You might also browse the [Cytoscape tutorials](http://tutorials.cytoscape.org/) and [RCy3 vignettes](https://bioconductor.org/packages/release/bioc/vignettes/RCy3/inst/doc/Overview-of-RCy3.html) if you want hands-on examples.

In this vignette, we will use the *CyTargetLinker* app for Cytoscape to extend a network representation of our pathway with drug-target interactions.

We need to load the latest drug-target database. The databases supported by **CyTargetLinker** are called *linksets* and can be downloaded from the [CyTargetLinker website](https://cytargetlinker.github.io/linksets/). We have provided an example **drugbank** linkset for this vignette, so you don't have to download anything.

```{r}
unzip(system.file("extdata","drugbank-5.1.0.xgmml.zip", package="rWikiPathways"), exdir = getwd())
drugbank <- file.path(getwd(), "drugbank-5.1.0.xgmml")
```

Now that we have our *drugbank* linkset  loaded, we can run CyTargetLinker as a command:
```{r}
commandsRun(paste0('cytargetlinker extend idAttribute="Ensembl" linkSetFiles="', drugbank, '"') )
commandsRun('cytargetlinker applyLayout network="current"')
```
This returns information about what was added. And in Cytoscape, you now have a copy of your original network, but now with additional nodes and edges. Check it out...

...Hmm, they are kind of plain and hard to see. Let's use Cytoscape visualization styles to fix that!
```{r}
my.drugs <- selectNodes("drug", by.col = "CTL.Type", preserve = FALSE)$nodes #easy way to collect node SUIDs by column value
clearSelection()
setNodeColorBypass(my.drugs, "#DD99FF")
setNodeShapeBypass(my.drugs, "hexagon")

drug.labels <- getTableColumns(columns=c("SUID","CTL.label"))
drug.labels <- na.omit(drug.labels)
mapply(function(x,y) setNodeLabelBypass(x,y), drug.labels$SUID, drug.labels$CTL.label)
```

Now we have drugs labeled as purple hexagons interacting with the network view of our WikiPathways hit from our functional enrichment analysis on a lung cancer dataset. Cool!  Hopefully, you can imagine now, extending this (or other pathawys) in other ways, e.g., TF and miRNA interactions. Or applying numerous other Cytoscape apps to the analysis and visualization of this result.

# Save
Last, but not least, be sure to save your work along the way.  
Session files save everything that is in Cytoscape, including the pathways, networks, data and styles. As with most project software, we recommend saving often!
```{r}
full.path=paste(getwd(),'tutorial_session',sep='/')
saveSession(filename=full.path) #.cys
```
Note: If you don’t specify a complete path, the files will be save relative to your Cytoscape installation directory, e.g., /Applications/Cytoscape_v3.6.1/… or somewhere you migt not have write permissions.

You can export extremely high resolution images, including vector graphic formats.
```{r}
full.path=paste(getwd(),'tutorial_image2',sep='/')
exportImage(filename=full.path, type='PDF') #.pdf
exportImage(filename=full.path, type='PNG', zoom=200) #.png; use zoom or width args to increase size/resolution
?exportImage
```