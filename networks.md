# Network analysis with Cytoscape


## Part 1: Cytoscape demo
Here, you will get a short introduction on how to use Cytoscape to further analyse your gene expression data. We will look at two different examples - I will provide the instructions for the user interface and the R code in case you want to automize it.

* [Extend pathway with drug-target interaction data](network1.md)
* [Analyse PPI network from up-regulated genes](network2.md)