## Investigating the molecular processes altered in lung cancer 

### Assignment 1: Browse the WikiPathways website

Check out the human cell cycle pathway page on WikiPathways:<br/>
[https://www.wikipathways.org/index.php/Pathway:WP179](https://www.wikipathways.org/index.php/Pathway:WP179)

*  How many different researchers contributed to this pathway?
*  When was the last edit to the pathway?
*  In the interactive pathway viewer, click on the *ORC1* gene in the bottom left corner. Cross-references are loaded. With which disease in OMIM is this gene associated?


----

### Assignment 2: Pathway analysis in PathVisio
First, you will use PathVisio to visualize the lung cancer transcriptomics dataset and perform pathway statistics to find pathways that are up- or down-regulated in lung cancer.

**Step 1: Open pathway in PathVisio**

*  In the pathvisio-3.3.0 folder, start PathVisio by executing the pathvisio.bat file (windows batch file).
* Open the cell cycle pathway in PathVisio (in the human pathway collection that you downloaded as instructed in the installation instructions.)
  *  Go to File → Open → Browse and select “Hs\_Cell\_Cycle\_WP179_106351” file

>  **Question 1: Literature references**<br/>Small numbers above data nodes, interactions or the info box in the top left of the pathway indicate publication references. Double click the info box in the top left (Title, Availability, Last modified, Organism) and go to the “Literature” tab.<br/> What are the title and authors of the paper referenced for this pathway?

>  **Question 2: Computer readable annotated pathway models**<br/>Click on the *GSK3B* gene in the top left. In the “Backpage” tab on the right side, you can find the annotation and cross references for the gene.<br/>With which identifier and database is this gene annotated?


You will not see any cross-references because we have not loaded the BridgeDb mapping file yet. 

----

**Step 2: Load identifier mapping database**
*  Go to Data → Select Gene Database → Browse to Hs\_Derby\_Ensembl_91.bridge
*  Check the status bar at the bottom to see if the gene database has been loaded correctly (nothing else will change in the program)

>  **Question 3: Computer readable annotated pathway models**<br/>Click on the *GSK3B* gene in the top left again. In the “Backpage” tab on the right side, you can find the annotation and cross references for the gene.</br>In the cross references, can you find the Ensembl, Entrez Gene and HGNC identifier for this gene?

----

**Step 3: Import the gene expression data in PathVisio**
*  Go to Data → Import expression data
*  Select the **lung-cancer-data.txt** file as the input file. Then click “Next”.
*  Make sure the correct data delimiter is set and you see the following preview. Then click “Next”.

![fig](images/Figure1.png "Figure 1")

> **Question 4: Gene identifier**<br/>The first column contains the identifier of the genes. From which of the three databases below are the identifiers in the dataset?<br/>- Ensembl<br/>- Entrez Gene<br/>- OMIM<br/> **(Required for following steps!)**

*  In the next dialog, you need to define the column containing the identifier and the database used. Select the “GeneID” column for the identifier. Based on the preview which “Database” needs to be selected? 

![alt text](images/Figure2.png "Figure 2")

*  The data will now be imported. Before clicking “Finish”, check how many rows were imported successfully and how many identifiers were not recognized. 

>  **Question 5: Data import**<br/>How many rows were imported successfully<br/>How many identifiers were not recognized? 

----

**<font color="red">Important!</font>**
If the number of rows is the same as the number of identifiers not recognized the data import was not done correctly - you probably did not select the correct database! Redo the import or ask one of the instructors for help.

If you clicked finish, you should see a default visualization on the pathway (if all genes are gray, the data import was not successful → please redo the import, make sure you select the correct database in the data import. Click on the *GSK3B* gene in the top left and check the “Data” tab on the right side → you should see that the *GSK3B* gene has a log2FC of -0.38?

----

**Step 4: Data visualization**
The default visualization is just a starting point to visualize your dataset; however if you want to explore your dataset in more detail, other visualization options are available and more suitable for the data nodes (in this case gene nodes).

> **Question 6: Log2FC values**<br/>In the lecture, you got a explanation of the dataset. Can you describe in your own words, what the log2FC means?<br/>In this example dataset, we compare primary lung cancer with healthy lung tissue. What does a positive or negative log2FC mean?

We will now create a visualization in PathVisio to visualize the log2FC as a gradient on the data nodes.

*  Go to Data → Visualization
*  Create a new visualization named “log2FC visualization” by clicking on the following edit icon in the top right (→ New)

![alt text](images/Figure3.png "Figure 3")

*  Select the checkbox before "Text label"
*  Select the checkbox before "Expression as color" and then the "Basic" option
*  Select the checkbox before "log2FC" and define a new color set (click on the edit icon for color set)
*  Select "Gradient" and define a gradient from -2 over 0 to 2 (blue – white – red) and click Ok

![alt text](images/Figure4.png "Figure 4")

> **Question 7: Biological interpretation**<br/>As a whole, is the pathway more up- or down-regulated in lung cancer? Is this expected?<b/>Make a screenshot of the pathway. What do the colors mean?

----

**Step 5: Pathway statistics**

We now know how the cell cycle pathway is altered in lung cancer, but there could be other interesting pathways to look at. How can we find those pathways? That’s when we perform pathway statistics to find those pathways that are more altered than expected (check lecture!).

* Go to Data → Statistics
* First, we want to find up-regulated pathways, so we need to define a criteria that selects all genes that are significantly up-regulated: 
`[log2FC] > 1 AND [P.Value] < 0.05`
* Then, we select the directory that contains all human pathways available in WikiPathways. All these pathways will be tested and statistically evaluated if they are up-regulated in our dataset: click on browse and select the "wikipathways-human-pathway-collection" folder
* Then, we click on "Calculate" and wait for the result table.

> **Question 8: Biological interpretation**<br/>What are the top five up-regulated pathways and what are their Z-scores?<br/>Do you see highly ranked pathways in the result table that you expected to be upregulated in lung cancer?

You can save the result table with all settings used as a .csv file, which can be opened in Excel.

* Repeat the same analysis for down-regulated genes → criteria = 
`[log2FC] < -1 AND [P.Value] < 0.05`

> **Question 8: Biological interpretation**<br/>What are the top five down-regulated pathways and what are their Z-scores?<br/>Do you see highly ranked pathways in the result table that you expected to be downregulated in lung cancer?



